# README #

Linux CNC Remote Controller

### What is this repository for? ###

* This repository contains file necessary to remotely control Linuxcnc on a BBB.

### How do I get set up? ###
* Make sure that `screen` is installed.
* put the script in the startup using the following command:
* run `crontab -e`
* add `@reboot sleep 20 &&  sudo /sbin/runuser machinekit -c "screen -dmS mkstartup; screen -S mkstartup -X stuff '/home/machinekit/rc/linuxcncrc/startall.sh\n'"`
* make sure that all of your scripts are in the following folder: `/home/machinekit/rc/linuxcncrc`
* With every reboot the script should run automatically


### How do I get set up manually? ###
* Run main.py
* It will run machinekit and then start listening to a port.
* If you prefer machinekit to run without a GUI you can set the display to "mkwrapper" instead of "axis" (or touchy or...)
* As soon as you run main.py it will connect to linuxcnc and try to `home_all`  
* The program will be listening to a port for commands.
* `main.py` will run `run.sh` which in turn will run `run.py`. It will generate a file named `log.txt` which has the output of run.py in it.
* `pid.txt` has the pid of machinekit to kill it at the end using `stop.sh` (which is called in `main.py`)
* It is confusing? well, it's not finished yet... and in the perfect world, all you need to do is to run `main.py` and the rest should take care of itself automatically.
* `ps.sh` is there to check if linuxcnc is running 
* `rm.sh` is there to remove the log files
* `internet.sh` is there to connect BBB to the internet

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
