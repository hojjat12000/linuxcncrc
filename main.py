#!/usr/bin/env python
# -*- coding: utf-8 -*-
import zmq
import signal
from subprocess import call
import os
import Config
import Commander
import time
import sys
class Network:
    """
    It will be listening to a port and receiving commands
    """

    def __init__(self):
        signal.signal(signal.SIGTERM, self.signal_term_handler)
        self.context = zmq.Context()
        self.server = self.context.socket(zmq.REP)
        try:
            self.server.bind("tcp://*:5456")
        except Exception as e:
            print "Exception is binding the server", e
            exit()
        print "Running linuxcnc...\n"
        self.run_machinekit(True)
        print "Commander...\n"
        self.commander = Commander.Commander()
        self.config = Config.Config()
        self.commands = self.config.get_commands()
        print("Numbr of commands found: " + str(len(self.commands)))
    
    def signal_term_handler(self,signal, frame):
        self.close()
        print "got SIGTERM"
        sys.exit(0)


    def run_machinekit(self,flag):
        if True:
            if flag:
                call(["./run.sh", ""])
            else:
                call(["./stop.sh", ""])

    def run(self):
        try:
            while True:
                try:
                    message = self.server.recv(flags=zmq.NOBLOCK)
                    print "message received: " , message
                    if message in self.commands:
                        print "command found in command files"
                        self.commander.run_file(self.commands[message])
                    elif message=="homeall":
                        self.commander.is_linuxcnc_running()
                        self.commander.home_all()
                    else:
                        self.commander.send(message)
                    typus, text = self.commander.get_error()
                    if typus=="OK":
                        self.server.send("Done!")
                    else:
                        self.server.send(typus + ":" + text)
                except zmq.Again as e:
                    pass
                time.sleep(0.5)
        except KeyboardInterrupt:
            print("W: interrupt received, stopping...")
        finally:
            # clean up
            self.close()

    def close(self):
        try:
            self.run_machinekit(False)
            self.server.close()
            self.context.term() 
        except Exception as ex:
            pass


    def __del__(self):
        self.server.close()
        self.context.term() 

net = Network()
net.run()
