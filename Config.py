import os
class Config:
    """
    this class loads all the config needed for the code
    It will also look for the command files (g code files)
    """
    def __init__(self):
        self.path = "/home/machinekit/rc/linuxcncrc/commands"
        self.extension = ".ngc"
        self.command_files ={}
        self.look_for_commands()

    def look_for_commands(self):
        try:
            for file in os.listdir(self.path):
                if file.endswith(self.extension):
                    com_addr = os.path.join(self.path, file)
                    com_name = file[:-len(self.extension)]
                    self.command_files[com_name] =  com_addr
        except Exception as e:
            print("No command loaded..."+str(e))

    def add_command(self, name, value):
        if name in self.command_files:
            pass
        else:
            self.command_files[name]=value

    def get_commands(self):
        return self.command_files


