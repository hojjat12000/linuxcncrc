import linuxcnc
import time

class Commander:
    """
    This class contains all the functions necessary for 
    sending commands to Linuxcnc
    """

    def __init__(self,shouldStart=True):
        self.init()
        self.status = "NotConnected"
        if shouldStart:
            self.start()

    def turnOn(self):
        self.c.state(linuxcnc.STATE_ESTOP_RESET)
        self.c.state(linuxcnc.STATE_ON)

    def isOn(self):
        self.s.poll()
        return not self.s.estop and self.s.enabled

    def home_all(self):
        self.ensure_mode(linuxcnc.MODE_MANUAL)
        self.c.home(-1)

    def home(self):
        i = 0
        for axis in self.s.axis:
            self.c.home(i)
            i = i + 1

    def isHomed(self):
        homed = True
        self.s.poll()
        for axis in self.s.axis:
            homed = homed and ((not axis['enabled']) or (axis['homed'] != 0))
        return homed

    def ok_for_mdi(self):
        self.s.poll()
        return self.isOn() and self.isHomed() and (self.s.interp_state == linuxcnc.INTERP_IDLE)

    def is_linuxcnc_running(self):
        print "Testing...\n"
        start_time = time.time()
        self.turnOn()
        self.c.wait_complete(2) # wait until mode switch executed
        while True:
            if time.time()-start_time>2:
                return False
            if self.isOn():
                return True
            time.sleep(0.4)

    def get_status(self):
        return self.status

    def init(self):
        self.s = linuxcnc.stat()
        self.c = linuxcnc.command()
        self.e = linuxcnc.error_channel()

    def start(self):
        # Try to connect
        start_time = time.time()
        status = "connecting"
        while True:
            if time.time()-start_time>120:
                print "Couldn't connect... giving up...\n"
                self.status = "failed"
                exit()
            if self.is_linuxcnc_running():
                print "Linuxcnc is running...\n"
                self.status = "connected"
                break
            self.init()
            time.sleep(0.5)
        print "Turned on\n"

        # Home all the motors and wait for them to be homed
        print "Homing...\n"
        start_time = time.time()
        self.home_all()
        while True:
            if time.time()-start_time>10:
                print "Homing took too long... quiting...\n"
                exit()
            if self.isHomed():
                break
            time.sleep(0.5)
        print "Homed\n"

    def get_error(self):
        error = self.e.poll()
        if error:
            kind, text = error
            typus = "info"
            if kind in (linuxcnc.NML_ERROR, linuxcnc.OPERATOR_ERROR):
                typus = "error"
            return typus, text
        return "OK", "OK"

    def send(self,commandStr):
        if self.ok_for_mdi():
            print "It is OK!"
            self.c.mode(linuxcnc.MODE_MDI)
            self.c.wait_complete() # wait until mode switch executed
            self.c.mdi(commandStr)
        else:
            print "Not OK for MDI"

    def run_file(self, file_addr):
        self.ensure_mode(linuxcnc.MODE_AUTO)
        #self.ensure_mode(linuxcnc.MODE_MANUAL)
        self.c.program_open(file_addr)
        self.c.auto(linuxcnc.AUTO_RUN, 0)

    def ensure_mode(self,m):
        self.s.poll() 
        if self.s.task_mode == m: return True
        self.c.mode(m)
        self.c.wait_complete()
        return True

